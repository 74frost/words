<?php

/**
 * @file
 * Contains \Drupal\words\Form\SettingsForm.
 */

namespace Drupal\words\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use \Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Configures words settings for this site.
 *
 * @internal
 */

class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
    
 public function getFormId() {
  return 'words_enable_settings';
}

  /**
   * {@inheritdoc}
   */
    
 protected function getEditableConfigNames() {
  return ['words_enable.settings',];
  }
    
  /**
   * {@inheritdoc}
   */
    
 public function buildForm(array $form, FormStateInterface $form_state) {
  $config = $this->config('words_enable.settings');
     
    // Global words settings.
     
  $form['words_enable_state'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Words Count'),
      '#default_value' => $config->get('enable_state'),
      '#description' => $this->t('Check this trigger to enable functionalyty of cout words in body of article and  '),];
    return parent::buildForm($form, $form_state);
  }

 /**
   * {@inheritdoc}
   */
 
 public function submitForm(array &$form, FormStateInterface $form_state) {
  $values = $form_state->getValues();
  $this->config('words_enable.settings')
  ->set('enable_state', $values['words_enable_state'])
  ->save();
  $confiq = \Drupal::config('words_enable.settings')->get('enable_state');
   
   if ($confiq == '0') {
    $config_factory = \Drupal::configFactory();
    $config = $config_factory->getEditable('core.entity_view_display.node.article.default');
    $config->set('hidden', ['field_words_counter' => true]);
    $config->clear('content.field_words_counter');
    $config->save();
    \Drupal::messenger()->addMessage(t('Module functionality are disabled, field of word count are hidden'), 'status');
    } 
     else {
      $config_factory = \Drupal::configFactory();
      $config = $config_factory->getEditable('core.entity_view_display.node.article.default');
      $config->set('content.field_words_counter', ['type' => number_integer, 'weight' => 4, 'region' => content, 'label' => above]);
      $config->set('content.field_words_counter.settings', ['prefix_suffix' => true, 'thousand_separator' =>'']);
      $config->set('content.field_words_counter.third_party_settings', []);
      $config->clear('hidden.field_words_counter');
      $config->save();
      \Drupal::messenger()->addMessage(t('Module functionality are enabled, field of word count are visible'), 'status');       
      }   
     drupal_flush_all_caches();
 }
}
